variable "extra_security_groups" {
  type    = "list"
  default = []
}

variable "key_name" {
  default = ""
}

variable "availability_zones" {}
variable "ec2_endpoint" {}

variable "es_cluster_name" {
  default = "elasticstack"
}

variable "vpc_id" {}
