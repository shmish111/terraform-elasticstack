variable "es_instances" {}
variable "es_instance_type" {}

variable "es_jvm_heap" {
  description = "heap size for es jvm, default to 500m"
  default     = "500m"
}

variable "es_config_file" {
  description = "Optional elasticsearch.yml config"
  default     = ""
}

data "template_file" "es_config" {
  template = "${var.es_config_file != "" ? var.es_config_file : file("${path.module}/templates/es_config.tpl")}"

  vars {
    cluster_name       = "${var.es_cluster_name}"
    ec2_groups         = "[\"elasticsearch\"]"
    availability_zones = "${var.availability_zones}"
    ec2_endpoint       = "${var.ec2_endpoint}"
  }
}

data "template_file" "es_jvm_config" {
  template = "${file("${path.module}/templates/es_jvm_config.tpl")}"

  vars {
    jvm_heap = "${var.kibana_es_jvm_heap}"
  }
}

data "template_file" "es_user_data" {
  template = "${file("${path.module}/templates/es_userdata.tpl")}"

  vars {
    es_config     = "${data.template_file.es_config.rendered}"
    es_jvm_config = "${data.template_file.es_jvm_config.rendered}"
    metricbeat_config = "${data.template_file.metricbeat_config.rendered}"
    access_key    = "${aws_iam_access_key.elasticsearch_ec2.id}"
    secret_key    = "${aws_iam_access_key.elasticsearch_ec2.secret}"
  }
}

data "template_cloudinit_config" "es_config" {
  gzip          = true
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.es_user_data.rendered}"
  }
}

resource "aws_instance" "es_node" {
  count         = "${var.es_instances}"
  ami           = "${data.aws_ami.es_base_ami.id}"
  instance_type = "${var.es_instance_type}"

  security_groups = ["${aws_security_group.elasticsearch.name}", "${concat(var.extra_security_groups)}"]

  key_name  = "${var.key_name}"
  user_data = "${data.template_cloudinit_config.es_config.rendered}"

  tags {
    ClusterName = "${var.es_cluster_name}"
  }
}
