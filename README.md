# Elastic Stack

The purpose of this terraform module is to provide a production ready elasticstack on AWS, this provides the following:

* Elasticsearch cluster
* Kibana Dash
* Monitoring with metricbeat
* Security with X-Pack (not implemented yet)

Each component is configured in such as way as to be ready to use in production, just scale appropriately.

## Usage

```hcl
provider "aws" {
  region = "eu-west-1"
}

terraform {
  backend "s3" {
    bucket  = "terraform-example-1"
    key     = "terraform/state"
    region  = "eu-west-1"
    profile = "my-profile"
  }
}

resource "aws_security_group" "ssh" {
  name        = "ssh"
  description = "Security Group for SSH to machines"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

module "elasticstack" {
  source = "./elasticstack"
  es_instances = 2
  es_instance_type = "t2.micro"
  key_name = "my-key-pair"
  availability_zones = "[\"eu-west-1b\"]"
  ec2_endpoint = "ec2.eu-west-1.amazonaws.com" # ES discovery needs to know the EC2 endpoint for your region
  extra_security_groups = ["${aws_security_group.ssh.name}"]
}
```

## TODO

* set up correct network architecture
* x-pack
* improvements to make it less likely to need to customize

### Bugs

metricbeat.yml seems to get executed by cloud-init after a certain point in the file, rather than just be saved to the correct location.
