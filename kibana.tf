variable "kibana_instances" {
  default = "1"
}

variable "kibana_instance_type" {}

variable "kibana_config_file" {
  description = "Optional kibana.yml config"
  default     = ""
}

variable "kibana_es_jvm_heap" {
  description = "heap size for es jvm, default to 500m"
  default     = "500m"
}

resource "aws_security_group" "kibana" {
  name        = "kibana"
  description = "Security Group for kibana machines"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }

  ingress {
    from_port   = 5601
    to_port     = 5601
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "kibana_config" {
  template = "${var.kibana_config_file != "" ? var.kibana_config_file : file("${path.module}/templates/kibana_config.tpl")}"
}

data "template_file" "kibana_es_config" {
  template = "${file("${path.module}/templates/kibana_es_config.tpl")}"

  vars {
    cluster_name       = "${var.es_cluster_name}"
    ec2_groups         = "[\"elasticsearch\"]"
    availability_zones = "${var.availability_zones}"
    ec2_endpoint       = "${var.ec2_endpoint}"
  }
}

data "template_file" "kibana_es_jvm_config" {
  template = "${file("${path.module}/templates/kibana_es_jvm_config.tpl")}"

  vars {
    jvm_heap = "${var.kibana_es_jvm_heap}"
  }
}

data "template_file" "kibana_user_data" {
  template = "${file("${path.module}/templates/kibana_userdata.tpl")}"

  vars {
    access_key           = "${aws_iam_access_key.elasticsearch_ec2.id}"
    secret_key           = "${aws_iam_access_key.elasticsearch_ec2.secret}"
    metricbeat_config    = "${data.template_file.metricbeat_config.rendered}"
    kibana_config        = "${data.template_file.kibana_config.rendered}"
    kibana_es_config     = "${data.template_file.kibana_es_config.rendered}"
    kibana_es_jvm_config = "${data.template_file.kibana_es_jvm_config.rendered}"
  }
}

data "template_cloudinit_config" "kibana_config" {
  gzip          = true
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.kibana_user_data.rendered}"
  }
}

resource "aws_instance" "kibana_node" {
  count         = "${var.kibana_instances}"
  ami           = "${data.aws_ami.es_base_ami.id}"
  instance_type = "${var.kibana_instance_type}"

  security_groups = ["${aws_security_group.kibana.name}", "${aws_security_group.elasticsearch.name}", "${concat(var.extra_security_groups)}"]

  key_name  = "${var.key_name}"
  user_data = "${data.template_cloudinit_config.kibana_config.rendered}"

  tags {
    ClusterName = "${var.es_cluster_name}"
  }
}
