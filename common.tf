resource "aws_security_group" "elasticsearch" {
  name        = "elasticsearch"
  description = "Security Group for elasticsearch machines"
  vpc_id      = "${var.vpc_id}"

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }

  ingress {
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 9300
    to_port     = 9300
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_ami" "es_base_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "description"
    values = ["Amazon Linux 2 LTS*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

resource "aws_iam_user" "elasticsearch_ec2" {
  name = "elasticsearch_ec2"
}

resource "aws_iam_access_key" "elasticsearch_ec2" {
  user = "${aws_iam_user.elasticsearch_ec2.name}"
}

resource "aws_iam_user_policy" "elasticsearch_ec2" {
  name = "elasticsearch_ec2"
  user = "${aws_iam_user.elasticsearch_ec2.name}"

  policy = <<EOF
{
    "Statement": [
        {
            "Action": [
                "ec2:DescribeInstances"
            ],
            "Effect": "Allow",
            "Resource": [
                "*"
            ]
        }
    ],
    "Version": "2012-10-17"
}
EOF
}

data "template_file" "metricbeat_config" {
  template = "${file("${path.module}/templates/metricbeat_config.tpl")}"
}
