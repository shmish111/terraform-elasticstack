set -e

# install elasticsearch
sudo yum -y install java-1.8.0-openjdk.x86_64
sudo rpm -i https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.2.rpm
sudo chkconfig --add elasticsearch
cd /usr/share/elasticsearch/
sudo bin/elasticsearch-plugin install --batch discovery-ec2

## Add jvm options
sudo cat <<EOF >/etc/elasticsearch/jvm.options
${kibana_es_jvm_config}
EOF

## Add elasticsearch config
sudo cat <<EOF > /etc/elasticsearch/elasticsearch.yml
${kibana_es_config}
EOF

## Add ec2 access and secret to the elasticsearch keystore
sudo /usr/share/elasticsearch/bin/elasticsearch-keystore create

echo ${access_key} | /usr/share/elasticsearch/bin/elasticsearch-keystore add --stdin discovery.ec2.access_key
echo ${secret_key} | /usr/share/elasticsearch/bin/elasticsearch-keystore add --stdin discovery.ec2.secret_key

## start elasticsearch
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo service elasticsearch restart

# Kibana

wget https://artifacts.elastic.co/downloads/kibana/kibana-6.2.2-x86_64.rpm
sha1sum kibana-6.2.2-x86_64.rpm 
sudo rpm --install kibana-6.2.2-x86_64.rpm
sudo chkconfig --add kibana

sudo systemctl daemon-reload
sudo systemctl enable kibana.service
sudo service kibana restart

sudo cat <<EOF >/etc/kibana/kibana.yml
${kibana_config}
EOF

# Metricbeat

curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-6.2.2-x86_64.rpm
sudo rpm -vi metricbeat-6.2.2-x86_64.rpm

sudo cat <<EOF > /etc/metricbeat/metricbeat.yml
${metricbeat_config}
EOF

### metricbeat command has to be run from config directory for some reason
cd /etc/metricbeat
/usr/share/metricbeat/bin/metricbeat modules enable elasticsearch system

sudo systemctl daemon-reload
sudo systemctl enable metricbeat.service
sudo service metricbeat restart